﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="height: 484px">
    <form id="form1" runat="server">
        <div style="height: 30px; width: 498px">
            ========== Operation 1: ========<br />
            <br />
            Number 1 :
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;<br />
            Number 2 :<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
&nbsp;<br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Add" />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Result Add"></asp:Label>
            <br />
            <br />
            <br />
            ======== Operation 2: ========<br />
            Number 1 :
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            Number 2 :
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Multiply" />
            <br />
            <asp:Label ID="Label2" runat="server" Text="Result mult"></asp:Label>
            <br />
            ======== Operation 3: ========<br />
            Number 1 :
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <br />
            Number 2 :
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Divide" />
            <br />
            <asp:Label ID="Label3" runat="server" Text="Result divide"></asp:Label>
        </div>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
