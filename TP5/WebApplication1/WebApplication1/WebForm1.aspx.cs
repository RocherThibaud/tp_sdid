﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1: System.Web.UI.Page
    {
        //declaration of a variable pointting to our type Web Service
        private com.dneonline.www.Calculator myWS;
        protected void Page_Load(object sender, EventArgs e)
        {
            myWS = new com.dneonline.www.Calculator();
        }



        protected void Button1_Click1(object sender, EventArgs e)
        {
            this.Label1.Text = myWS.Add(
                int.Parse(this.TextBox1.Text),
                int.Parse(this.TextBox2.Text)
            ).ToString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            this.Label2.Text = myWS.Multiply(
                int.Parse(this.TextBox3.Text),
                int.Parse(this.TextBox4.Text)
            ).ToString();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            this.Label3.Text = myWS.Divide(
                int.Parse(this.TextBox5.Text),
                int.Parse(this.TextBox6.Text)
            ).ToString();
        }
    }
}