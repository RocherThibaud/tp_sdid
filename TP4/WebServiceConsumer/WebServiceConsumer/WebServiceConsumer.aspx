﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebServiceConsumer.aspx.cs" Inherits="WebServiceConsumer.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label2" runat="server" Text="======= Opération 1 ======="></asp:Label>
        </div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Hello World!" />
        <p>
            <asp:Label ID="LabelResult1" runat="server" Text="Result"></asp:Label>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="======= Opération 2 ======="></asp:Label>
        </p>
        <asp:Label ID="LabelName" runat="server" Text="Name"></asp:Label>
        <p>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </p>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Invoke hello with name" />
        <p>
            <asp:Label ID="LabelResult2" runat="server" Text="Result Op2"></asp:Label>
        </p>
        <p>
            <asp:Label ID="Label8" runat="server" Text="======= Opération 3 ======="></asp:Label>
        </p>
        <asp:Label ID="Label9" runat="server" Text="P1: "></asp:Label>
&nbsp;<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
&nbsp;+&nbsp;&nbsp;
        <asp:Label ID="Label10" runat="server" Text="P2: "></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <p>
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Invoke add P1 + P2" />
        </p>
        <asp:Label ID="LabelResult3" runat="server" Text="Result Op3"></asp:Label>
    </form>
</body>
</html>
